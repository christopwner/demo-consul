# consul service discovery and mesh

## requirements
* minikube
* kubectl
* helm

## overview
consul is a tool from hashicorp to simplify networking and configuration for multi-service environments. it provides a range of toolsets from simple service discovery and binding to dynamic app configuration and mTLS. 

## deploy consul
the first step for deployment is to startup a local k8s cluster (minikube) and install consul from their helm chart with our configuration set in [values](values.yml)
```
minikube start --memory 4096 #start minikube
helm repo add hashicorp https://helm.releases.hashicorp.com #setup helm repo
helm install -f values.yml #install consul
```

#### values
* Set the prefix used for all resources in the Helm chart to `consul`
* Name the Consul datacenter dc1
* Configure the datacenter to run only 1 server
* Configure the server to use the root user
* Enable the Consul UI and expose it via a NodePort
* Enable Consul service mesh features by setting connectInject.enabled to true
* Enable Consul service mesh CRDs by setting controller.enabled to true

#### discovery
once consul is deployed, it can be accessed through `minikube service consul-ui` or the appropriate port-forward (if not using minikube). in the consul ui, the home page will be `services` which is a registry of all services in consul. 


basic overview of service discovery since dawn of time:
```mermaid
flowchart LR
1((requester)) <--bind--> 2((provider)) -.publish.-> 3((registry))
1 -.find.-> 3
```

nodes are encouraged to install the consul-agent as it can provide health checks, but documentation asserts not a requirement.

## deploy services
Deploy two services one for `counting` the amount of gets, and a `dashboard` for querying gets from `counting` on a timer. 
```
kubectl apply -f counting.yml
kubectl apply -f dashboard.yml
```

### transparent proxies (enabled by default)
every service is deployed with a sidecar proxy (envoy), that will handle all traffic. when transparent proxies are enabled, apps will not have to define upstreams as they're implicity determined by the service intentions (explicit upstreams are still supported however)

flow of traffic with bypass `counting.svc.cluster.local:9001`
```mermaid
flowchart LR

subgraph Pod A
direction TB
  1(dashboard) --> 2(envoy)
end

subgraph Pod B
direction BT
  3(envoy) --> 4(counting)
end

1 -.bypass.-> 4
2 --> 3
```

## visit dashboard
open a port with k8s to view the `dashboard` deployment. 

```kubectl port-forward deploy/dashboard 9002:9002```

by default, intentions are set to `allow` which means all services will be accessable to all other services. visit the deployment at [localhost:9002](http://localhost:9002)

## intentions
intentions are access control for services and are used to control which services may establish connections or make requests. intentions could be set through crd, cli or ui.

run ```kubectl apply -f deny.yml``` to create an intention to disable access for `dashboard` to `counting`
